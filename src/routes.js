//import Cadastro from '';
import Home from './components/home/Home';
const Cadastro = () => import('./components/cadastro/Cadastro');

export const routes = [

    { path: '/', name: 'home', component: Home, titulo: 'Home', menu: true },
    { path: '/cadastro', name: 'cadastro', component: Cadastro, titulo: 'Cadastro', menu: true },
    { path: '/cadastro/:id', name: 'alterar', component: Cadastro, menu: false },
    { path: '*', component: Home, menu: false }

];